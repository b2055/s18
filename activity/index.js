let trainer = {
	Name: "Ash Ketchum",
	Age: 10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbusaur"],
	Friends: 
	{ 
		Hoenn : ["May", "Max"], 
		Kanto : ["Brock", "Misty"] 
	},
	talk: function(pokemon){
		console.log(pokemon + "! I Choose you!")
	}
}

console.log(trainer)
console.log("Result of dot notation")
console.log(trainer.Name)
console.log("Result of bracket notation")
console.log(trainer['Pokemon'])
console.log("Resultof talk method")
console.log(trainer.talk(trainer.Pokemon[0]))

function Pokemon(name, level) {
	this.name = name,
	this.level = level
	this.health = 2 * level;
	this.attack = level;


	this.faint = function(name) {
		console.log(name + " fainted")
	}

	this.tackle = function(target) {
		target.health = target.health - this.attack 
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + target.health)

		if (target.health <= 0) {
			this.faint(target.name)
		}
	}

}

let Pikachu = new Pokemon("Pikachu", 12)
let Geodude = new Pokemon("Geodude", 8)
let Mewtwo = new Pokemon("Mewtwo", 100)

console.log(Pikachu)
console.log(Geodude)
console.log(Mewtwo)

Geodude.tackle(Pikachu)
console.log(Pikachu)

Mewtwo.tackle(Geodude)

console.log(Geodude)