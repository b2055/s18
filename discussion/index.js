let grades = [98.5, 94.3, 89.2, 90.1]


let grade = {
	// object initializer/literal notation
	// - objects consist of properties, which are used to describe an onject. Key-value pair
	math: 89.2,
	english: 98.5,
	science: 90.1,
	filipino: 94.3
}

// we used dot notation to access the properties/value of our object
console.log(grade.english)

//Syntax:

/*
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}
*/

let cellphone = {
	brand: "Nokia 3310",
	color: "Dark Blue",
	manufactureDate: 1999
}


console.log(typeof cellphone)

let student = {
	firstName: "John",
	lastName: "Smith",
	mobileNo: 091234567890,
	location: {
		city: "Tokyo",
		country: "Japan"
	},
	email: ['john@mail.com', 'johnsmith@mail.xyz'],
	// object method(function inside an object)
	fullName: function() {
		return this.firstName + ' ' + this.lastName
	}
}

// dot notation
console.log(student.location.city)
// bracket notation
console.log(student['firstName'])

console.log(student.email)
console.log(student.email[0])

console.log(student.fullName())

const person1 = {
	name: "Jane",
	greeting: function() {
		return "Hi I'm " + this.name
	}
}

console.log(person1.greeting())


// array of objects
let contactList = [
	{
		firstName: "John",
		lastName: "Smith",
		location: "Japan",
	},
	{
		firstName: "Jane",
		lastName: "Smith",
		location: "Japan",
	},
	{
		firstName: "Jasmine",
		lastName: "Smith",
		location: "Japan",
	}
]


console.log(contactList[0].firstName)


let people = [
	{
		name: "Juanita",
		age: 13
	},
	{
		name: "Juanito",
		age: 14
	}
]


people.forEach(function(person){
	console.log(person.name)
})

console.log(`${people[0].name} are the list`)

// Creating objects using a Constructor Function (JS OBJECT CONSTRUCTORS/OBJECT FROM BLUEPRINTS)

// Creates a reusable function to create several objects that have the same data structure
// This is useful for creating multiple copies/instances of an object

//Object literals
// let object = {}
// instance - distinct/unique objects
// let object = new object
// An instance is a concrete occurence of any object which emphasizes on the unique identiy
/*
	Syntax:

	function ObjectName(keyA, keyB) {
		this.keyA = keyA,
		this.keyB = keyB
	} 
*/

function Laptop(name, manufactureDate) {
	// the "this" keyword allows to assign a new object's property
	// by associating them with values received from our parameter
	this.name = name,
	this.manufactureDate = manufactureDate
}

// This is a unique instance of the Laptop object

let laptop = new Laptop("Lenovo", 2008);

console.log("Result from creating objects using object constructor")
console.log(laptop)

// This is another unique instance of the laptop object
let myLaptop = new Laptop("MacBook Air", 2020)
console.log("Result from creating objects using object constructors")
console.log(myLaptop)

let oldLaptop = Laptop("Portal R2E CCMC", 1980)
console.log("Without new keyword")
console.log(oldLaptop)  


// Creating empty objects
let computer = {}
let myComputer = new Object();


// name of the Macbook using the dot notation and bracket notation

// dot notation
console.log(myLaptop.name)
// bracket notation
console.log(myLaptop['name'])

let array = [laptop, myLaptop]

console.log(array[0].name)

// Initializing/Adding/Deleting/Reassigning Object Properties

// Initialized/added properties after the object was created

// This is useful for times when an object's properties are undetermined at the time of creating them

let car = {}

// add an object properties, we can use dot notation

car.name = "Honda Civic"
console.log(car)

car["manufacture date"] = 2019;
console.log(car)

// reassigning object properties
car.name = "Dodge Charger R/T"
console.log(car)

// Deleting object properties
delete car["manufactureDate"]
console.log("result from deleting properties")
console.log(car)


// OBJECT METHODS
// A mthod is a function which is a property of an object
// They are also functions and one of the key differences they have is that methods are functions related to a specified object

let person = {
	name: "john",
	talk: function() {
		console.log("Hello my name is " + this.name)
	}
}

console.log(person)
console.log("Result from object methods")
person.talk()

// Adding methods object person

person.walk = function() {
	console.log(this.name + ' walked 25 steps forward')
}

person.walk();


let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
	emails: ["joe@mail.com", "joesmith@mail.xyz"],,
	introduce: function(){
		console.log("Hello my name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce();

/*
	Real World Application of Objects
	-Scenario

		1. We would like to create a game that would have several pokemon interact with each other
		2. Every pokemon would have the same set of stats, properties and functions
*/


// Using object literals to create multiple kinds of pokemon would be time consuming

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
 	tackle: function() {
 		console.log("This pokemon tackled target Pokemon")
 		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
 	},
 	faint: function() {
 		console.log("pokemon fainted")
 	}
}

// Creating an object constructor instead of object literals.

function Pokemon(name, level) {
	this.name = name,
	this.level = level
	this.health = 2 * level;
	this.attack = level;

	//methods

	this.tackle = function(target) {
		console.log(this.name + ' tackled' + target.name)
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	}

	this.faint = function() {
		console.log(this.name + "fainted")
	}

}


let pikachu = new Pokemon("Pikachi", 16)
let charizard = new Pokemon("Charizard", 8)

pikachu.tackle(charizard)


